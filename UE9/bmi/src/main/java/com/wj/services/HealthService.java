/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wj.services;

/**
 *
 * @author deaven
 */
public class HealthService {
    public static float getBmi(float weight, float height) {
        return weight / (height/100 * height/100); 
    }

    public static String evaluateBmi(float bmi) {
        if(bmi < 18.5) {
            return "Underweight";
        } else if (bmi < 24.9) {
            return "Normal";
        }

        return "Overweight";
    }
}
