/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wj.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author deaven
 */
@Named(value = "bmiHistoryModel")
@SessionScoped
public class BmiHistoryModel implements Serializable {
    
    private static List<BmiModel> bmis = new ArrayList<BmiModel>();
    private BmiModel selectedBmi;

    public static List<BmiModel> getBmis() {
        return bmis;
    }

    public static void add(BmiModel model) {
        bmis.add(model);
    }
    
    public BmiModel getSelectedBmi() {
        return selectedBmi;
    }
    
    public String requestEdit(BmiModel model) {
        this.selectedBmi = model;
        
        return "bmiEdit";
    }
    
    public String saveChanges(BmiModel model) {
        
        
        return "bmiResult";
    }
}
