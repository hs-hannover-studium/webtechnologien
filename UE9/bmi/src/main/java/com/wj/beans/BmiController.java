/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wj.beans;

import com.wj.services.HealthService;
import java.util.Date;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

/**
 *
 * @author deaven
 */
@Named(value = "bmiController")
@RequestScoped
public class BmiController {

    public String submit(BmiModel model) {
        float bmiValue = HealthService.getBmi(model.getWeight(), model.getHeight());
        String bmiEvaluation = HealthService.evaluateBmi(bmiValue);
        
        model.setBmi(bmiValue);
        model.setBmiEvaluation(bmiEvaluation);
        model.setTestDay(new Date());
        
        BmiModel.setHighScore(bmiValue);
        BmiHistoryModel.add(model);
        
        return "bmiResult";
    }
    
    /**
     * Validator Source
     * https://www.journaldev.com/7035/jsf-validation-example-tutorial-validator-tag-custom-validator
     * @param context
     * @param comp
     * @param value 
     */
    public void validateWeight(FacesContext context, UIComponent comp,
                    Object value) {

            Float weight = (Float) value;

            if (weight < 4) {
                ((UIInput) comp).setValid(false);

                FacesMessage message = new FacesMessage(
                                "Your weight is not reasonable");
                context.addMessage(comp.getClientId(context), message);
            }

    }
    
    public void validateHeight(FacesContext context, UIComponent comp,
                    Object value) {

            Float height = (Float) value;

            if (height > 230) {
                ((UIInput) comp).setValid(false);

                FacesMessage message = new FacesMessage(
                                "Your height is not reasonable");
                context.addMessage(comp.getClientId(context), message);
            }

    }
    
}
