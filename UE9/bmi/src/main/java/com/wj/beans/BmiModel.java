/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wj.beans;

import java.util.Date;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author deaven
 */
@Named(value = "bmiModel")
@RequestScoped
public class BmiModel {
    
    private static float highScore;

    private float height;
    private float weight;
    private float bmi;
    private String bmiEvaluation;
    private Date testDay;

    public static float getHighScore() {
        return highScore;
    }

    public static void setHighScore(float newScore) {
        if(BmiModel.highScore > newScore) {
            return;
        }
        
        BmiModel.highScore = newScore;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getBmi() {
        return this.bmi;
    }
    
    public void setBmi(float bmi) {
        this.bmi = bmi;
    } 
    
    public String getBmiEvaluation() {
        return this.bmiEvaluation;
    }
    
    public void setBmiEvaluation(String bmiEvaluation) {
        this.bmiEvaluation = bmiEvaluation;
    }

    public Date getTestDay() {
        return testDay;
    }

    public void setTestDay(Date testDay) {
        this.testDay = testDay;
    }
}
