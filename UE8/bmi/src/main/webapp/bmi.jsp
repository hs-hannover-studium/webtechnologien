<%-- 
    Document   : bmi
    Created on : May 7, 2021, 5:49:22 PM
    Author     : deaven
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "com.wj.bmi.services.HealthService"%>
<%@page import = "com.wj.bmi.services.ValidationService"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="app.css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="main">
            <section>
                <% if(!ValidationService.IsFloat(request.getParameter("height")) ||
                      !ValidationService.IsFloat(request.getParameter("weight"))) { %>

                
                <div class="alert error">
                    <div class="alert__content">
                        <h1>Wrong input</h1>
                    </div>
                    <div class="alert__action">
                        <a href="/bmi" class="btn">Back</a>
                    </div>
                </div>

                <% } else {

                    float weight = Float.parseFloat(request.getParameter("weight"));
                    float height = Float.parseFloat(request.getParameter("height"));

                    float bmi = HealthService.GetBmi(weight, height);
                    String bmiEvaluation = HealthService.EvaluateBmi(bmi);
                %>
                <div class="card">
                    
                    <div class="card__title">
                        <div>Your results!</div>
                    </div>

                    <div class="card__body">
                        <p>Bmi: <%= bmi %></p>
                        <p>You are <%= bmiEvaluation %></p>
                        <p><a href="/bmi" class="btn">Back</a></p>
                    </div>

                </div>
        
                <% } %>             
            </section>
        </div>
        
    </body>
</html>
