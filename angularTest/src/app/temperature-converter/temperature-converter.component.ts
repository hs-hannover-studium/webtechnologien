import { Component, OnInit } from '@angular/core';
import { TemperatureService } from '../temperature.service';

@Component({
  selector: 'app-temperature-converter',
  templateUrl: './temperature-converter.component.html',
  styleUrls: ['./temperature-converter.component.css']
})
export class TemperatureConverterComponent implements OnInit {

  public celcius: number;

  constructor(private temperatureService: TemperatureService) {
    this.celcius = 0;
    this.temperatureService = temperatureService;
  }

  ngOnInit(): void {
  }

  getFahrenheit(): string {
    return "Hello";
  }

}
