import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';

import Bmi from 'src/app/models/bmi';

@Component({
  selector: 'app-bmi-result',
  templateUrl: './bmi-result.component.html',
  styleUrls: ['./bmi-result.component.css']
})
export class BmiResultComponent implements OnInit {

  public bmi: Bmi;

  constructor(private store: StoreService) {
    this.bmi = this.store.bmi;
  }

  ngOnInit(): void {
    console.log(this.store.bmi);


    this.bmi = this.store.bmi;
  }

}
