import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Bmi from 'src/app/models/bmi';

import { HealthService } from 'src/app/services/health.service';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-bmi',
  templateUrl: './bmi.component.html',
  styleUrls: ['./bmi.component.css']
})
export class BmiComponent implements OnInit {

  public bmiForm: FormGroup;

  constructor(private healthService: HealthService, private store: StoreService, private router: Router) {
    this.bmiForm = new FormGroup({
      height: new FormControl('',
        {
          validators: Validators.required,
          updateOn: 'change'
        }
      ),
      weight: new FormControl('',
        {
          validators: Validators.required,
          updateOn: 'change'
        }
      )
    })
  }

  ngOnInit(): void {
  }

  get height() {
    return this.bmiForm.get("height");
  }

  get weight() {
    return this.bmiForm.get("weight");
  }

  onSubmit() {
    this.healthService.getBmi(this.height?.value, this.weight?.value).then(data => {
      const bmi: Bmi = {
        bmi: data.bmi,
        evaluation: data.evaluation
      }

      this.store.bmi = bmi;

      this.router.navigateByUrl("/bmiResult");
    });
  }
}
