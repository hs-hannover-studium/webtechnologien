import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationbarComponent } from './components/navigationbar/navigationbar.component';
import { BmiComponent } from './pages/bmi/bmi.component';
import { BmiResultComponent } from './pages/bmi-result/bmi-result.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationbarComponent,
    BmiComponent,
    BmiResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
