import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BmiComponent } from './pages/bmi/bmi.component';
import { BmiResultComponent } from './pages/bmi-result/bmi-result.component';

const routes: Routes = [
  { path: '', redirectTo: '/bmi', pathMatch: 'full' },
  { path: "bmi", component: BmiComponent },
  { path: "bmiResult", component: BmiResultComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
