export default interface Bmi {
    bmi: number;
    evaluation: string;
} 