import { Injectable } from '@angular/core';
import Bmi from '../models/bmi';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  public bmi: Bmi;

  constructor() { 
  }
}
