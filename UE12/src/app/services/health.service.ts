import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Bmi from '../models/bmi';

@Injectable({
  providedIn: 'root'
})
export class HealthService {

  constructor() { }

  public getBmi(height: number, weight: number): Promise<Bmi> {
    return fetch(`http://localhost:8080/api/v1/bmi/calculate?height=${height}&weight=${weight}`)
      .then(response => response.json())
      .catch((error) => {
        console.error('Error:', error);
      });
  }

  public evaluateBmi(bmi: Bmi) {
    if (bmi.bmi < 18.5) {
      return "Underweight";
    } else if (bmi.bmi < 24.9) {
      return "Normal";
    }

    return "Overweight";
  }
}
