const Bmi = {
    Calculate: (height, weight) => {
        if(isNaN(height) || isNaN(weight)) 
            throw "Wrong Input"

        return weight / (height / 100 * height / 100);
    },
    Evaluate: (bmi) => {
        if (bmi < 18.5) {
            return "Underweight";
        } else if (bmi < 24.9) {
            return "Normal";
        }

        return "Overweight";
    }
}

module.exports = Bmi;
