const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const app = express()
const baseRoute = '/api/v1'

// Routes
const bmiRoutes = require('./routes/bmi')

// Middleware
app.use(cors())
app.use(bodyParser.json())
app.use(`${baseRoute}/bmi`, bmiRoutes)

app.listen(8080)