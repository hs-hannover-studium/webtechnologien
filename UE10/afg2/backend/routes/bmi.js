const express = require('express')
const router = express.Router()

const bmiService = require('../services/bmiService')

router.get('/', (req, res) => {
    res.send("Works!")
})

router.get('/calculate', (req, res) => {
    try {
        const bmiValue = bmiService.Calculate(req.query.height, req.query.weight).toFixed(2).toString()
        const bmiEvaluation = bmiService.Evaluate(bmiValue)

        const bmi = {
            bmi: bmiValue,
            evaluation: bmiEvaluation
        }

        res.json(bmi)
    } catch (err) {
        res.status(404).json(err)
    }
})

module.exports = router