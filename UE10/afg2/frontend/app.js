const form = document.getElementById('bmi-form')
const formSubmitBtn = document.getElementById('submit-bmi')
const result = document.getElementById('bmi-result')
const evaluation = document.getElementById('bmi-evaluation')

formSubmitBtn.addEventListener("click", function (event) {
    event.preventDefault()
    form.checkValidity()

    if(!form.reportValidity())
        return

    const height = form.height.value
    const weight = form.weight.value

    fetch(`http://localhost:8080/api/v1/bmi/calculate?height=${height}&weight=${weight}`)
        .then(response => response.json())
        .then(data => {
            result.innerHTML = data.bmi
            evaluation.innerHTML = data.evaluation
        })
        .catch((error) => {
            console.error('Error:', error);
        });
});