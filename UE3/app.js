let orderBtn = document.getElementById("submit-order");
let pizzaForm = document.getElementById("pizza-order");
let orderTbody = document.getElementById("order-tbody");
let totalSum = document.getElementById("total-sum");
let totalOrders = document.getElementById("total-orders");
let orders = [];

orderBtn.addEventListener('click', (e) => {
    e.preventDefault();

    let order = {
        size: "",
        ingredients: [],
        price: 0,
        calories: 0
    }

    order.size = pizzaForm.size.value;

    switch (order.size) {
        case 'mini':
            order.price += 2.5;
            order.calories += 500;
            break;
        case 'maxi':
            order.price += 3.5;
            order.calories += 625;
            break;
        case 'party':
            order.price += 4.5;
            order.calories += 800;
            break;
        default:
            break;
    }

    for (ingredient of pizzaForm.ingredients) {
        if (ingredient.checked) {
            order.ingredients.push(ingredient.value);
            order.price += 0.5;
            order.calories += 50;
        }
    }

    if(order.ingredients.length <= 0) {
        alert("Bitte Zutaten auswählen!");
        return;
    }

    submitOrder(order);
    updatePrice();

    totalOrders.innerHTML = orders.length;
})

/**
 * 
 * @param {object} order 
 */
function submitOrder(order) {
    let row = orderTbody.insertRow();
    let cell;
    let text;

    orders.push(order);

    cell = row.insertCell();
    text = document.createTextNode(order.size);
    cell.appendChild(text);

    cell = row.insertCell();
    text = document.createTextNode(order.ingredients);
    cell.appendChild(text);

    cell = row.insertCell();
    text = document.createTextNode(order.price + " €");
    cell.appendChild(text);

    cell = row.insertCell();
    text = document.createTextNode(order.calories);
    cell.appendChild(text);

    orderTbody.append(row);
}

function updatePrice() {
    let total = 0;

    for(order of orders) {
        total += order.price;
    }

    totalSum.innerHTML = total;
}